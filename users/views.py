import logging

from django.contrib.auth import get_user_model

from rest_framework import viewsets

from .serializers import UserSerializer
from .tasks import check_user_age


logger = logging.getLogger(__name__)


class UserViewSet(viewsets.ModelViewSet):
    """
    REST API for users table.
    """
    lookup_field = 'username'
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer

    def perform_create(self, serializer):
        super().perform_create(serializer)
        check_user_age.apply_async(args=(serializer.instance.id, ))
