import datetime

from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist

from celery.utils.log import get_task_logger

from api.celery import celery_app


logger = get_task_logger(__name__)


@celery_app.task(autoretry_for=[ObjectDoesNotExist], retry_kwargs={'max_retries': 5})
def check_user_age(user_id):
    """
    Check is user older than 18's.
    """
    user = get_user_model().objects.get(id=user_id)
    today = datetime.datetime.today()
    birth_date = user.birth_date
    age = today.year - birth_date.year - ((today.month, today.day) < (birth_date.month, birth_date.day))
    if age < 18:
        logger.warning('Young user registered.')
        user.is_active = False
        user.save()
