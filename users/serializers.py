from django.contrib.auth import get_user_model
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    """
    User serializer.
    """
    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'birth_date', 'confirm_tos', 'is_active')
        read_only_fields = ('id', 'is_active')
