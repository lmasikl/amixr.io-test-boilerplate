import uuid
from django.contrib.auth.models import AbstractUser
from django.db import models



class User(AbstractUser, models.Model):
    """
    User model.
    """
    username = models.CharField(unique=True, max_length=32)
    birth_date = models.DateField()
    confirm_tos = models.BooleanField(default=False)
