import time
import datetime

from django.urls import reverse


def test_users_resource(client, headers, adult_user_data, young_user_data):
    for user_data, result in ((adult_user_data, True), (young_user_data, False)):
        list_url = reverse('v1:user-list')
        response = client.post(list_url, data=user_data, **headers)
        assert response.status_code == 201, response.json()
        user = response.data
        assert user['is_active'] is True
        detail_url = reverse('v1:user-detail', kwargs={'username': user['username']})
        response = client.get(detail_url, **headers)
        assert response.status_code == 200
        assert response.data['is_active'] == result, response.json()
