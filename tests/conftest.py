import datetime
import pytest
import uuid

from django.contrib.auth import get_user_model


@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    pass


@pytest.fixture
def adult_user():
    """ Test adult user. """
    return get_user_model().objects.create(
        username=uuid.uuid4().hex,
        confirm_tos=True,
        birth_date=datetime.datetime(2000, 1, 1)
    )

@pytest.fixture
def young_user():
    """ Test young user. """
    return get_user_model().objects.create(
        username=uuid.uuid4().hex,
        confirm_tos=True,
        birth_date=datetime.datetime.today()
    )


@pytest.fixture
def adult_user_data():
    """ Test adult user data. """
    return {
        "birth_date": datetime.datetime(2000, 1, 1).strftime('%Y-%m-%d'),
        "username": "adult"
    }

@pytest.fixture
def young_user_data():
    """ Test young user data. """
    return {
        "birth_date": datetime.datetime.today().strftime('%Y-%m-%d'),
        "username": "young"
    }


@pytest.fixture
def headers():
    """ HTTP request headers. """
    return {
        'Content-Type': 'application/json'
    }
