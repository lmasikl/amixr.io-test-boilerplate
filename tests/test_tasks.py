from users.tasks import check_user_age


def test_check_young_user_age(young_user):
    model = young_user.__class__
    check_user_age(young_user.id)
    assert model.objects.get(id=young_user.id).is_active is False


def test_check_adult_user_age(adult_user):
    model = adult_user.__class__
    check_user_age(adult_user.id)
    assert model.objects.get(id=adult_user.id).is_active is True
