import os

import environ
from celery import Celery

# Set environment for clean start
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'api.settings')

# Django environ
root = environ.Path(__file__) - 2
environ.Env.read_env(root('.env'))

# Celery app init
celery_app = Celery('celery_app')
celery_app.config_from_object('django.conf:settings', namespace='CELERY')

# Auto-discover celery tasks
celery_app.autodiscover_tasks()
