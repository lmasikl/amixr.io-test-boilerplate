default: run

run:
	@docker-compose up --build -d


test:
	@docker-compose -f docker-compose.tests.yml up --build
	@docker-compose -f docker-compose.tests.yml down

makemigrations:
	@pipenv run ./manage.py makemigrations

migrate:
	@pipenv run ./manage.py migrate

run-dev:
	@pipenv run ./manage.py runserver
